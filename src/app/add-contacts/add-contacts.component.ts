import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ContactServiceService } from '../services/contact-service.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-contacts',
  templateUrl: './add-contacts.component.html',
  styleUrls: ['./add-contacts.component.css']
})
export class AddContactsComponent implements OnInit {

  reactiveForm: any = FormGroup;
  public selectedFile: any = File;

  addContact(submitForm: FormGroup){
    const contact = submitForm.value;
    const formData = new FormData();
    formData.append('contact', JSON.stringify(contact));
    console.log(contact);
    formData.append('file', this.selectedFile);
    console.log(formData.get('contact').toString());
    this.contactService.addContact(formData).subscribe((response) => { console.log(response) });

  }
  onSelectFile(event) {
    const file = event.target.files[0];
    console.log(file)
    this.selectedFile = file;
  }



  constructor(private http: HttpClient, private contactService: ContactServiceService, private fb: FormBuilder) { 
    this.reactiveForm = this.fb.group({
      displayname : new FormControl('',Validators.required),
      email : new FormControl('',Validators.required),
      firstname : new FormControl('',Validators.required),
      lastname : new FormControl('',Validators.required),
      note : new FormControl('',Validators.required)

    });
  }

  ngOnInit() {
  }

}
