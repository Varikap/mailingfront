import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FileUploadModule } from 'ng2-file-upload';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule }    from '@angular/common/http';

import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { FileuploadComponent } from './fileupload/fileupload.component';

import { AddContactsComponent } from './add-contacts/add-contacts.component';
import { EditContactsComponent } from './edit-contacts/edit-contacts.component';
import { SendMailComponent } from './send-mail/send-mail.component';
import { RegisterComponent } from './register/register.component';
import { RecieveMailComponent } from './recieve-mail/recieve-mail.component';

import { FileServiceService } from './services/file-service.service';
import { RegisterServiceService } from './services/register-service.service';
import { ContactServiceService } from './services/contact-service.service';
import { MailServiceService } from './services/mail-service.service';
import { SentMailComponent } from './sent-mail/sent-mail.component';
import { UnreadComponent } from './unread/unread.component';
import { AddAccountComponent } from './add-account/add-account.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    FileuploadComponent,
    AddContactsComponent,
    EditContactsComponent,
    SendMailComponent,
    RegisterComponent,
    RecieveMailComponent,
    SentMailComponent,
    UnreadComponent,
    AddAccountComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    MatFormFieldModule,
    ReactiveFormsModule ,
    FileUploadModule
  ],
  providers: [FileServiceService, RegisterServiceService, ContactServiceService, MailServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
