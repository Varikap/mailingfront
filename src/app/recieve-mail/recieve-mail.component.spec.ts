import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecieveMailComponent } from './recieve-mail.component';

describe('RecieveMailComponent', () => {
  let component: RecieveMailComponent;
  let fixture: ComponentFixture<RecieveMailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecieveMailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecieveMailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
