import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MailServiceService } from '../services/mail-service.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-recieve-mail',
  templateUrl: './recieve-mail.component.html',
  styleUrls: ['./recieve-mail.component.css']
})
export class RecieveMailComponent implements OnInit {

  private emails: Array<any> = [];
  private folderName: string = "all";

  selectedEmail: any;
  onSelect(data: any): void {
    this.selectedEmail = data;
    console.log(this.selectedEmail);
}
  constructor(private http: HttpClient,private mailService: MailServiceService) { }

  ngOnInit() {
    this.mailService.recieveMail(this.folderName).subscribe(data => { this.emails = data; console.log(this.emails[0].firstname) });
  }

}
