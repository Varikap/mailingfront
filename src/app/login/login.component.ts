import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { FormControl, Validators } from '@angular/forms';
import { AuthServiceService } from '../services/auth-service.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = new FormControl('', Validators.required);
  password = new FormControl('', Validators.required);
  errorMessage: string = '';
  authenticationFlag: boolean = true;

  getErrorMessageu() {
    return this.username.hasError('required') ? 'You must enter a value' :
      this.username.hasError('username') ? 'Not a valid username' :
        '';
  }
  getErrorMessagep() {
    return this.password.hasError('required') ? 'You must enter a value' :
      this.password.hasError('password') ? 'Not a valid username' :
        '';
  }

  constructor(private authService: AuthServiceService, private router: Router) { }

  token: string;
  response: string;

  login(username, password) {
    console.log("USERNAME JE >>> " + username.value);
    console.log("PASS >>>>>>>" + password.value);
    this.authService.login(username.value, password.value).subscribe(token => {
      this.router.navigate(['/home']); console.log("TOKEN JE >>>>>>>>>>>>>" + token.token)
    },
      (err) => { console.log(err) });
  }
  logins(username, password) {
    console.log("USERNAME JE >>> " + username.value);
    sessionStorage.removeItem('response')
    console.log("PASS >>>>>>>" + password.value);
    this.authService.login(username.value, password.value).subscribe(response => {
      if (response == "400") {
        this.authenticationFlag = false;
        return;
      }
    },
      (token) => {
        this.router.navigate(['/home']); console.log("TOKEN JE >>>>>>>>>>>>>" + token.token)
      });
  }

  ngOnInit() {
  }

}
