import { Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FileServiceService } from '../services/file-service.service';

@Component({
  selector: 'app-fileupload',
  templateUrl: './fileupload.component.html',
  styleUrls: ['./fileupload.component.css']
})
export class FileuploadComponent implements OnInit {

  // reactiveForm: any = FormGroup;
  public selectedFile: any = File;

  onSelectFile(event) {
    const file = event.target.files[0];
    console.log(file)
    this.selectedFile = file;
  }
  // saveForm() {

  //     // const filiziren = submitForm.value;
  //     const formData = new FormData();
  //     formData.append('file', this.selectedFile);
  //     this.fileService.savePicture(formData).subscribe((response) => { console.log(response) });


  // }

  ngOnInit(): void {

  }

  constructor(private http: HttpClient, private fileService: FileServiceService) { }


}
