import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MailServiceService } from '../services/mail-service.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-unread',
  templateUrl: './unread.component.html',
  styleUrls: ['./unread.component.css']
})
export class UnreadComponent implements OnInit {

  private emails: Array<any> = [];
  private folderName: string = "all";
  constructor(private http: HttpClient,private mailService: MailServiceService) { }

  ngOnInit() {
    this.mailService.recieveMail(this.folderName).subscribe(data => { this.emails = data; console.log(this.emails[0].firstname) });
  }

}
