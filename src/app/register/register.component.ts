import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RegisterServiceService } from '../services/register-service.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  reactiveForm: any = FormGroup;

  registerMe(submitForm: FormGroup){
    const user = submitForm.value;
    const formData = new FormData();
    // formData.append('user', user);
    console.log(user);
    this.registerService.saveUser(user).subscribe((response) => { console.log(response) });
    this.router.navigate(['/login']);

  }

  constructor(private http: HttpClient, private registerService: RegisterServiceService, private fb: FormBuilder, private router: Router) { 
    this.reactiveForm = this.fb.group({
      firstname : new FormControl('',Validators.required),
      lastname : new FormControl('',Validators.required),
      username : new FormControl('',Validators.required),
      password : new FormControl('',Validators.required)

    });
  }

  ngOnInit() {
  }

}
