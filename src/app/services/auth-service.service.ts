import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Token } from '../model/Token';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  constructor(private http: HttpClient) { }

  login(username: string, password: string) {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    return this.http.post<Token>('http://192.168.0.14:8080/api/login', { username, password }, httpOptions).pipe(catchError(this.handleError), tap(res => this.setResponse(res.status)), tap(res => this.setSession(res.token)));
}

private setSession(token: string) {
  console.log(token);
  sessionStorage.setItem("token", token);
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
  private setResponse(status: string) {
    console.log(status);
    sessionStorage.setItem("response", status);
  }

private log(res:string) {
  return res;
}
jeste = true;
isUserLoggedIn() {
  let user = sessionStorage.getItem('token');
  console.log(user);
  if (user === null){
    this.jeste = false;
  }
  return this.jeste;
}
userHasAccount(): any {
  let token = sessionStorage.getItem('token');
  const httpOptions = {
    headers: new HttpHeaders({
      'X-Auth-Token': token
    })
  };
  return this.http.get('http://192.168.0.14:8080/api/account/hasAccount', httpOptions);

}
getLoggedIn(): any {
  let token = sessionStorage.getItem('token');
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Auth-Token': token
    })
  };
  //let headers = new HttpHeaders();
  //headers.append('Content-Type', 'application/json');
  //let myParams = new URLSearchParams();
  //myParams.append('X-Auth-Token', token);
  return this.http.get('http://192.168.0.14:8080/api/loggedin/', httpOptions);
}



}
