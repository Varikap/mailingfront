import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ContactServiceService {

  constructor(public http: HttpClient) {
    this.http = http;
   }
   addContact(formData: FormData): Observable<any>{
    let token = sessionStorage.getItem('token');
    console.log(token);
    const httpOptions = {
      headers: new HttpHeaders({
        'X-Auth-Token': token
      })
    };

    return this.http.post('http://192.168.0.14:8080/api/contact', formData, httpOptions);
   }
   editContact(formData: FormData): Observable<any>{
    let token = sessionStorage.getItem('token');
    console.log(token);
    console.log("sadasdasda" + formData);
    const httpOptions = {
      headers: new HttpHeaders({
        'X-Auth-Token': token
      })
    };

    return this.http.put('http://192.168.0.14:8080/api/contact', formData, httpOptions);
   }

   getUsersContacts(): Observable<any>{
    let token = sessionStorage.getItem('token');
    console.log(token);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Auth-Token': token
      })
    };

    return this.http.get('http://192.168.0.14:8080/api/contact/all', httpOptions);
   }
   }

