import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class FileServiceService {

  constructor(public http: HttpClient) {
    this.http = http;
  }
  savePicture(formData: FormData, contactid): Observable<any> {
    let token = sessionStorage.getItem('token');
    console.log(token);
    console.log(contactid);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Auth-Token': token
      })
    };
    return this.http.post('http://192.168.0.14:8080/api/photo', {formData, contactid}, httpOptions);
  }
  saveAttachment(formData: FormData): Observable<any> {
    return this.http.post('http://192.168.0.14:8080/api/attachment', formData);
  }
}
