import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})

export class RegisterServiceService {

  constructor(public http: HttpClient) { 
    this.http = http;
  }
  saveUser(user): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({ 'content-type': 'application/json;charset=UTF-8' })
    };
    return this.http.post('http://192.168.0.14:8080/api/user', user, httpOptions);
  }
}
