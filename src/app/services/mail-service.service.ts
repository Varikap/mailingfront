import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MailServiceService {

  constructor(public http: HttpClient) {
    this.http = http;
  }
  sendMail(formData: FormData): Observable<any> {
    let token = sessionStorage.getItem('token');
    console.log(token);
    const httpOptions = {
      headers: new HttpHeaders({
        'X-Auth-Token': token
      })
    };

    return this.http.post('http://192.168.0.14:8080/api/message/attachment', formData, httpOptions);
  }

  recieveMail(folderName: string): Observable<any> {
    let token = sessionStorage.getItem('token');
    console.log(token);
    console.log(folderName);
    const httpOptions = {
      headers: new HttpHeaders({
        'X-Auth-Token': token
      })
    };

    return this.http.get('http://192.168.0.14:8080/api/message/all?folderName='+folderName, httpOptions);
  }
}
