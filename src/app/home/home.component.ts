import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../services/auth-service.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private account: any;
  constructor(private authService: AuthServiceService, private router: Router) { }

  logout(){
    sessionStorage.removeItem("token")
    this.router.navigate(['/login']);
  }
  ngOnInit() {
    if (this.authService.isUserLoggedIn()==false){
      this.router.navigate(['/login']);

    }
    console.log(this.authService.getLoggedIn());
    this.authService.userHasAccount().subscribe(data => { this.account = data; console.log("DATA JE: " + data);
    if (data == null){
      this.router.navigate(['/add-account']);

    }
   });
  }

}
