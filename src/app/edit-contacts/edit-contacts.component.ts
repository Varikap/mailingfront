import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ContactServiceService } from '../services/contact-service.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-contacts',
  templateUrl: './edit-contacts.component.html',
  styleUrls: ['./edit-contacts.component.css']
})
export class EditContactsComponent implements OnInit {

  reactiveForm: any = FormGroup;
  private kontakti: Array<any> = [];
  public selectedFile: any = File;

  selectedKontakt: any;
  onSelect(data: any): void {
    this.selectedKontakt = data;
    console.log(this.selectedKontakt);
}
onSelectFile(event) {
  const file = event.target.files[0];
  console.log(file)
  this.selectedFile = file;
}

  editContact(selectedKontakt){
    const formData = new FormData();
    formData.append('contact', JSON.stringify(selectedKontakt));
    formData.append('file', this.selectedFile);
    console.log(selectedKontakt.id);
    this.contactService.editContact(formData).subscribe((response) => { console.log(response) });

  }

  constructor(private http: HttpClient, private contactService: ContactServiceService, private fb: FormBuilder, private router: Router) { 
  }
  ngOnInit() {
    this.contactService.getUsersContacts().subscribe(data => { this.kontakti = data; console.log(this.kontakti[0].firstname) });
  }

}
