import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { EditContactsComponent } from './edit-contacts/edit-contacts.component';
import { AddContactsComponent } from './add-contacts/add-contacts.component';
import { FileuploadComponent } from './fileupload/fileupload.component';
import { RegisterComponent } from './register/register.component';
import { SendMailComponent } from './send-mail/send-mail.component';
import { SentMailComponent } from './sent-mail/sent-mail.component';
import { UnreadComponent } from './unread/unread.component';
import { RecieveMailComponent } from './recieve-mail/recieve-mail.component';
import { AddAccountComponent } from './add-account/add-account.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: 'home', component: HomeComponent},
  { path: 'add-contact', component: AddContactsComponent},
  { path: 'edit-contact', component: EditContactsComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'fileupload', component: FileuploadComponent },
  { path: 'inbox', component: RecieveMailComponent },
  { path: 'unread', component: UnreadComponent },
  { path: 'sent', component: SentMailComponent },
  { path: 'sendMail', component: SendMailComponent },
  { path: 'add-account', component: AddAccountComponent },
  { path: '', component: LoginComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
