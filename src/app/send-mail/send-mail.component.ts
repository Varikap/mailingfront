import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MailServiceService } from '../services/mail-service.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-send-mail',
  templateUrl: './send-mail.component.html',
  styleUrls: ['./send-mail.component.css']
})
export class SendMailComponent implements OnInit {

  reactiveForm: any = FormGroup;
  public selectedFiles: any = File;
  to = new FormControl('', Validators.required);
  subject = new FormControl('', Validators.required);

  getErrorMessageTo() {
    return this.to.hasError('required') ? 'You must enter a value' :
      this.to.hasError('to') ? 'Not a valid username' :
        '';
  }
  getErrorMessageSubject() {
    return this.subject.hasError('required') ? 'You must enter a value' :
      this.subject.hasError('subject') ? 'Not a valid subject' :
        '';
  }

  sendEmail(submitForm: FormGroup) {
    const message = submitForm.value;
    const formData = new FormData();
    //{ type: 'application/json' }
    formData.append('message', JSON.stringify(message));
    if ((typeof (this.selectedFiles) === "object")) {
      for (let f of this.selectedFiles) {
        formData.append('files', f);
      }
    }
    console.log(formData.get('message'));
    this.mailService.sendMail(formData).subscribe((response) => { this.router.navigate(['/home']); console.log(response) });

  }

  onSelectFile(event) {
    const file = event.target.files;
    console.log(file)
    this.selectedFiles = file;
  }

  constructor(private http: HttpClient, private mailService: MailServiceService, private fb: FormBuilder, private router: Router) {
    this.reactiveForm = this.fb.group({
      to: new FormControl('', Validators.required),
      subject: new FormControl('', Validators.required),
      content: new FormControl('', Validators.required)

    });
  }

  ngOnInit() {
    console.log(this.selectedFiles.length > 0);
  }

}
